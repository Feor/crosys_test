﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CROSYS_TEST.Models.support
{
    /** Ассоциативный массив
     * 
     * реализуется через добавление в List записей,
     * состоящих из Dictionary с перегруженным на прием текстовых ключей индексеромы
     * 
     * <see cref="http://ru.stackoverflow.com/a/126382/185776 "/>
     */
    public class Record
    {
        private Dictionary<string, string> _properties;

        public Record()
        {
            _properties = new Dictionary<string, string>();
        }

        public string this[string name] // перегруженный индексер
        {
            get
            {
                return _properties[name];
            }
            set
            {
                _properties[name] = value;
            }
        }
    }
}