﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using CsvFile;

namespace CROSYS_TEST.Models.support
{   
    /**
     * Загружает(исключительно загружает!) CSV файл в ассоциативный массив(или не ассоциативный). Первая строчка файла считается названиями полей.
     * 
     * <KnownIssues>:
     * 1) В случае, если какое-то поле пропущено, но не обозначено разделительным символом, то выкинет exeption.
     * 2) Нет ограничения на загружаемые файлы, т.е. можно попытаться загрузить какой-нибудь файл с 100 000 000 + записей, и получить неожиданный exeption
     * 2.1) Нет оптимизации хранения данных в памяти. Все хранится в массиве, что не эффективно совсем
     * 
     * <@property> string CSVFile путь к загружаемому файлу
     * //<@property> string? Encodding кодировка загружаемого файла(не реализуется в этой версии)
     * <@property> string Delimiter символ разделителя полей
     * //<@property> bool ReturnAssoc=true по умолчанию возвращается ассоциативный массив
     * 
     * <@property> int counter Колличество загруженных строк 
     * 
     * <@return> string[] data
     */
    public class CSVLoader:IDataLoader
    {
        public int Counter { get; set; }
        public string CSVFile { get; set; }
        //public string? Encoding { get; set; } //Кодировка
        public char[] Delimeter { get; set; } //Разделительный символ
        //public bool ReturnAssoc { get; set;}//Возвращать ассоциативный массив?
        public string[] Fields { get; set; }//Имена полей
        public List<Record> Records { get; set; } //Ассоциативный массив с загруженными данными
        //public CSVList[] Records { get; set; }

        public CSVLoader() {
            this.Delimeter = new char[] {','};
            //this.ReturnAssoc = true;
            this.Counter = 0;
            this.Records = new List<Record>();
        }

        public CSVLoader(string file):this() {
            this.CSVFile = file;
            this.load();
        }

        public void load(string file)
        {
            this.CSVFile = file;
            this.load();
        }

        public void load() {
            if (this.CSVFile == null || !File.Exists(this.CSVFile)) throw new Exception("No file to load! Set <CSVFile> property!");

            List<string> columns = new List<string>();
            using (var reader = new CsvFileReader(this.CSVFile))
            {
                reader.ReadRow(columns);
                this.Fields = columns.ToArray();//Заполняем массив названий полей (наивно предпологаем, что в первой строчке названия полей)
                this.Counter = 0;

                while (reader.ReadRow(columns))
                {
                    string[] values = columns.ToArray();
                    if (values.Length != this.Fields.Length) throw new Exception("Error while rendering file :"+this.CSVFile+" at line: "+this.Counter+" !");//Проверка на "целостность"

                    int i = 0;
                    var rec = new Record();
                    
                    foreach (string key in this.Fields)
                    {
                        rec[key] = values[i++];
                    }

                    this.Records.Add(rec);

                    this.Counter++;
                }

            }
        }//load()
    }//class CSVLoader 
}