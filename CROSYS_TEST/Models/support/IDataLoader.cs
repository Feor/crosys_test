﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CROSYS_TEST.Models.support
{
    /**
     * Interface для классов, загружающих данные из внешних источников или хранилищ
     * 
     * <@param>
     * 
     */
    public interface IDataLoader
    {
        int Counter { get; set; } //Колличество записей
        string[] Fields { get; set; } //Названия полей
        List<Record> Records { get; set; } //Данные полей в ассоциативном массиве (ReadOnly)

        void load(); //Загрузка данных из предпологаемого по умолчанию источника
    }

}