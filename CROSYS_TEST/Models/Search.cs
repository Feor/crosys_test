﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using CROSYS_TEST.Models.support;
using System.Web.Mvc;

namespace CROSYS_TEST.Models
{

    public class Search : IDataErrorInfo
    {
        //public SelectListItem[] Structure { get; set; }

        public string SearchField { get; set; }
        public string SearchRequest { get; set; }
        public string SearchType { get; set; }

        public string[] Fields { get; set; }
        public Record[] SearchResult { get;private set; }

        public string Error { get { return null; } }

        public Search() {
            //getFields();
            SearchType = "like";
            SearchField = getFields()[0].Value;
        }

        public SelectListItem[] getFields()
        {
         CSVLoader fields = new CSVLoader( AppDomain.CurrentDomain.BaseDirectory + @"/data/structure.csv");

         SelectListItem[] Struc = new SelectListItem[fields.Records.Count];
            int i = 0;
            foreach (Record field in fields.Records)
            {
                Struc[i++] = new SelectListItem { Text = field[fields.Fields[2]], Value = field[fields.Fields[0]] };
            }
            Struc[0].Selected = true;

            return Struc;
        }

    #region Поиск
    //Реализуется два типа поиска - по вхождению и по точному совпадению
    //для начала загрузим CSV файл в ассоциативный массив
    public void search()
    {
        Validation();
            CSVLoader data = new CSVLoader(AppDomain.CurrentDomain.BaseDirectory + @"/data/data.csv");
            Fields = data.Fields;
            if (SearchType == "exact") //точный поиск
            {
                var res = from Result in data.Records
                          where Result[SearchField] == SearchRequest
                          select Result;

                SearchResult = res.ToArray();
            }
            if (SearchType == "like") //поиск "содержит"
            {
                var res = from Result in data.Records
                          where Result[SearchField].Contains(SearchRequest)
                          select Result;

                SearchResult = res.ToArray();
            }
    }

#endregion
    #region Валидация данных
        public string this[string property]
        {
            get
            {
                if ((property == "SearchField")   && string.IsNullOrEmpty(SearchField))   return "Введите поле для поиска";
                if ((property == "SearchType")    && string.IsNullOrEmpty(SearchType))    return "Введите тип поиска";
                if ((property == "SearchRequest") && string.IsNullOrEmpty(SearchRequest)) return "Введите текст для поиска";
                
                return null;
            }
        }
        private void Validation()
        {
            var Validator = new[] { "SearchField", "SearchRequest", "SearchType" };
            bool isValid = Validator.All(x => this[x] == null);
            if (!isValid)
                throw new InvalidOperationException("Неправильный поисковый запрос!");
        }
        #endregion

    }
}