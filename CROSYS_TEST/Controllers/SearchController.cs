﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CROSYS_TEST.Models;

namespace CROSYS_TEST.Controllers
{
    public class SearchController : Controller
    {
        //
        // GET: /Search/
        [HttpGet]
        public ViewResult Index(Search data)
        {
            ViewData["Fields"] = data.getFields();
            return View("Index",data);
        }

        //
        // POST: /Search/Result
        [HttpPost]
        public ViewResult Result(Search data)//, Fields fields)
        {
            ViewData["Fields"] = data.getFields();
            if (ModelState.IsValid)
            {
                data.search();
                return View("Result", data);
            }
            else 
            {
                return View("Index", data);
            }
        }

    }
}
